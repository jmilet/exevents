defmodule EventProducerTest do
  use ExUnit.Case
  doctest Exevents

  setup do
    Detqueue.init(EventProducerTestETS)
    EventProducer.start_link(EventProducerTestETS)
    :ok
  end

  test "insert value works" do
    while_not fn ->
      "New Value 0" == Detqueue.get_value(EventProducerTestETS, 5)
    end
  end

  def while_not(condition) do
    if not condition.() do
      while_not(condition)
    end
  end
end
