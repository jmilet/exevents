defmodule DetqueueTest do
  use ExUnit.Case
  doctest Exevents

  test "insert value works" do
    table = :test
    Detqueue.init(table)
    assert 1 == Detqueue.insert_value(table, "hola que tal")
    assert 2 == Detqueue.insert_value(table, "hola que tal, otra vez")
    assert "hola que tal" == Detqueue.get_value(table, 1)
    assert "hola que tal, otra vez" == Detqueue.get_value(table, 2)
  end

  test "returns a range" do
    table = :test2
    Detqueue.init(table)
    for i <- 1..20, do: Detqueue.insert_value(table, "hola que tal #{i}")
    assert [[10, "hola que tal 10"], [11, "hola que tal 11"]] == Detqueue.get_range(table, 10, 2)
  end

  test "returns a range, check nil values are filtered" do
    table = :test3
    Detqueue.init(table)
    for i <- 1..6, do: Detqueue.insert_value(table, "hola que tal #{i}")
    assert [[5, "hola que tal 5"], [6, "hola que tal 6"]] == Detqueue.get_range(table, 5, 20)
  end

  test "returns empty list when asked for zero elements" do
    table = :test4
    Detqueue.init(table)
    for i <- 1..6, do: Detqueue.insert_value(table, "hola que tal #{i}")
    assert [] == Detqueue.get_range(table, 1, 0)
  end

  test "returns just one element when asked one elementº" do
    table = :test5
    Detqueue.init(table)
    for i <- 1..6, do: Detqueue.insert_value(table, "hola que tal #{i}")
    assert [[2, "hola que tal 2"]] == Detqueue.get_range(table, 2, 1)
  end
end
