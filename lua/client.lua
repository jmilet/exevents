
--[[
https://github.com/luarocks/luarocks/wiki/Using-LuaRocks

luarocksinstall httpclient
luarocks install lua-cjson
eval $(luarocks path --bin)

lua client.lua 1 10
]]

local hc = require('httpclient').new()
local cjson = require('cjson')
local posix = require('posix')

function get_last_element(t)
  local newTable = {table.unpack(t)}
  table.sort(newTable, function(a, b) return a[1] < b[1] end)
  return newTable[#newTable][1]
end

function build_string_from_table(t)
  local res = {}
  for _,v in pairs(t) do
    if type(v) == "table" then
      build_string_from_table(v)
    else
      table.insert(res, v)
    end
  end
  return table.concat(res, "|")
end

function now_string()
  return os.date("%Y-%m-%d %X")
end

function log(text)
  print(string.format("%s|%s", now_string(), text))
end

function table.empty(t)
  return next(t) == nil
end

function main(from, amount)
  local url = string.format("http://localhost:4001/events/%d/%d", from, amount)
  --print(url)

  local res = hc:get(url)
  if res.body then
    --print(res.body)
    local data = cjson.decode(res.body)

    if not table.empty(data) then
      local result = build_string_from_table(data)
      local last = get_last_element(data)
      --posix.sleep(1)
      log(string.format("%s|%s|Result len %d, last value %d, next value %d", url, result, #data, last, last + 1))
      main(last + 1, amount)
    else
      posix.sleep(3)
      main(from, amount)
    end
  else
    print(res.err)
  end
end

from, to = arg[1], arg[2]
main(from, to)
