defmodule EventProducer do
  use GenServer

  @max_wait_seconds 2

  def start_link(ets) do
    GenServer.start_link(__MODULE__, %{
      random_module: Application.get_env(:exevents, :random_module, :rand),
      ets: ets
    })
  end

  def init(state) do
    schedule_work(0)
    {:ok, state}
  end

  def handle_info(:work, state) do
    work(state.ets, state.random_module)
    schedule_work(state.random_module.uniform(@max_wait_seconds * 1000))
    {:noreply, state}
  end

  defp work(ets, random_module) do
    Detqueue.insert_value(ets, "New Value #{random_module.uniform(10000)}")
  end

  defp schedule_work(n) do
    Process.send_after(self(), :work, n)
  end

end
