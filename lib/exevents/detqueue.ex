defmodule Detqueue do

  @counter_key "cnt"

  def init(name) do
    :ets.new(name, [:public, :named_table, :set])
  end

  def insert_value(name, value) do
    # We know that an incremental counter is a bottle neck, but it
    # simplies our proof of concept.
    key = get_key(name)
    :ets.insert(name, {key, value})
    key
  end

  def get_value(name, key) do
    case :ets.lookup(name, key) do
      [] -> nil
      [{_key, value}] -> value
    end
  end

  def get_range(name, from, amount) do
    to = from + amount - 1
    if from <= to do
      (for i <- from..to, do: [i, get_value(name, i)])
        |> Enum.filter(fn [_key, value] -> value != nil end)
    else
      []
    end
  end

  defp get_key(name) do
    :ets.update_counter(name, @counter_key, 1, {1, 0})
  end
end
