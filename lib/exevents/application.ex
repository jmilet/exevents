defmodule Exevents.Application do
  @moduledoc false

  use Application
  import Supervisor.Spec

  def start(_type, _args) do
    Detqueue.init(:events)

    children = generate_event_producers() ++ cowboy()

    opts = [strategy: :one_for_one, name: Exevents.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp generate_event_producers do
    for worker <- 1..100, do: worker(EventProducer, [:events], id: worker)
  end

  defp cowboy do
    [Plug.Adapters.Cowboy.child_spec(:http, ExeventsHttpRouter, [], [port: 4001])]
  end
end
