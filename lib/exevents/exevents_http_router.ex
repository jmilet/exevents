defmodule ExeventsHttpRouter do
  use Plug.Router

  plug :match
  plug :dispatch

  get "/events/:from/:amount" do
    events = Detqueue.get_range(:events, String.to_integer(from), String.to_integer(amount))
    send_resp(conn, 200, events |> Poison.encode!)
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
