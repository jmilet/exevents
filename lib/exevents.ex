defmodule Exevents do
  @moduledoc """
  Documentation for Exevents.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Exevents.hello
      :world

  """
  def hello do
    :world
  end
end
